FROM debian:buster-slim

LABEL maintainer="Paul Puschmann <paul.puschmann@gmail.com>"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y -o APT::Install-Suggests=0 -o APT::Install-Recommends=0 apt-transport-https ca-certificates openssl && \
    apt install -y -o APT::Install-Suggests=0 -o APT::Install-Recommends=0 nginx-light ffmpeg libnginx-mod-rtmp && \
    rm -rf /var/lib/apt/lists/* && \
    rm -r /var/cache/apt/archives/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    mkdir -p /opt/streaming/hls && \
    mkdir -p /opt/streaming/dash && \
    mkdir -p /var/www/streaming && \
    mkdir -p /var/www/xsl && \
    chown www-data: /var/www -R && \
    chown www-data: /opt/streaming -R

# Set up config file
COPY etc/nginx.conf /etc/nginx/nginx.conf

EXPOSE 1935
CMD ["nginx", "-g", "daemon off;"]

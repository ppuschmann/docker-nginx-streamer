# nginx-streamer

## build & run

Either use `docker-compose` or do:

1. `docker build . -t nginx-streamer`
2. `docker run --rm --name nginx -v $PWD/etc/nginx.conf:/etc/nginx/nginx.conf:ro -v $PWD/html:/var/www/html -v $PWD/movies:/var/www/streaming -p 80:80 nginx-streamer`

## media

Put videos for playback into the folder `movies`.
For now the file `ard.mp4` is hardcoded into `dash-vod.html`.
